# O que é Git Flow?
Git Flow é uma extensão do Git, trata-se de um modelo de organização de branches.

# Versionamento
Para manter um padrão mais organizado para versionar, nasceu um projeto chamado também de [SemVer](http://semver.org/), que é um sistema bem básico. Ele é controlado pelo formato **x.y.z**:

- **x**: fica para as famosas Major Versions, que seriam as versões principais fechadas.
- **y**: fica para as Minor Versions, que seriam adições de pequenas melhorias.
- **z**: fica para os patchs, que seriam correções de bugs.

Então, se tivéssemos, por exemplo, a versão 1.4.11, significaria que ela já teve sua primeira versão lançada, 4 novas features adicionadas depois e 11 correções.

## master
A branch **master** é onde fica o código em nível de **produção**. Todo código novo produzido, eventualmente será juntado com a branch **master** durante o desenvolvimento;

## develop
Resumo: A branch **develop** está como um ambiente de **Homologação**, enquantão a branch **master** está para um ambiente de **Produção**.

A branch **develop** contém o código em nível preparatório para o próximo deploy. Quando novas features são terminas, elas são juntadas com esta branch para serem testadas (em conjunto, no caso de mais de uma feature), e somente depois as atualizações da branch **develop** passam por mais um processo para então serem juntadas com a branch **master**.

## feature/*
São branches no qual são desenvolvidos recursos novos para o projeto em questão. Essas branches tem por convenção nome começando com **feature**/ (exemplo: **feature/new-layout**) e são criadas a partir da branch **develop** (pois um recurso pode depender diretamente de outro recurso em algumas situações), e, ao final, são juntadas com a branch **develop**;

## hotfix/*
São branches no qual são realizadas correções de bugs críticos encontrados em ambiente de produção, e que por isso são criadas a partir da branch **master**, e são juntadas diretamente com a branch **master** e com a branch **develop** (pois os próximos deploys também devem receber correções de bugs críticos, certo?). Por convenção, essas branches tem o nome começando com **hotfix/** e terminando com o próximo sub-número de versão (exemplo: **hotfix/2.31.1**), normalmente seguindo as regras de algum padrão de versionamento, como o padrão do versionamento semântico, que abordei neste post;

## release/*
São branches com um nível de confiança maior do que a branch **develop**, e que se encontram em nível de preparação para ser juntada com a branch **master** e com a branch **develop** (para caso tenha ocorrido alguma correção de bug na branch **release/*** em questão). Note que, nessas branches, bugs encontrados durante os testes das features que vão para produção podem ser corrigidos mais tranquilamente, **antes** de irem efetivamente para produção. Por convenção, essas branches tem o nome começando com **release/** e terminando com o número da próxima versão do software (seguindo o exemplo do hotfix, dado acima, seria algo como **release/2.32.0**), normalmente seguindo as regras do versionamento semântico, como falado acima;


--------------

# Passos de Desenvolvimento
1- A primeira coisa que você deve fazer é criar um novo branch chamado `develop`. É aqui que todo o código em desenvolvimento vai ficar, você pode chamar ele de homologa se quiser.

```
// Comando para criar um novo branch
$ git checkout -b 'develop'
```

PS: Lembre-se que tudo o que estiver na branch `develop` deverá ser testado antes de dar merge para a branch `master`;

2- Agora você vai criar uma nova feature no projeto, pode ser qualquer coisa. Tanto um novo botão de chamada para uma página nova, quanto uma view ou um gif. Antes de mais nada, você deve criar um branch especifico para features:

```
$ git checkout -b 'feature/index'
```

3- No final do desenvolvimento dessa feature, e sabendo que tudo está ocorrendo bem. Chega a hora de dar um marge com a branch `develop` com o comando.
Primeiro você deve ir para a branch `develop` com o comando: `git checkout develop`

Agora é só dar o merge:

```
$ git merge feature/index
```

Olha ae, agora ta tudo na branch `develop` e ta funcionando de boa? Então agora é só para a branch `master` e dar um merge. Mas deu ruim? Não funcionou? Então ta na hora de um hotfix.

4- Aqui o processo é praticamente igual;
```
$ git checkout -b 'hotfix/index'
```

Você cria um novo branch para o hotfix e faz as alterações para rodar aquela sua feature defeituosa. Consertou? Bora pro develop e dar um merge pra testar!
```
$ git checkout develop
$ git merge hotfix/index
```

Agora sim, tudo ocorreu bem! Bora dar um merge com a master e tacar pra produção!

```
$ git checkout master
$ git merge develop
```

Ta ae! Tudo funcionou, e não deu mais merda com ninguem (y)